---
tags:
    - Java
    - Minecraft
---

# Simple Gems Fabricated

Simple Gems Fabricated est une modification du jeu Minecraft utilisant l'API Fabric. 

## Fonctionalités

- Modification du terrain (Ajout de nouvaux minerais : Rubis/Saphir/Opale)
- Ajout d'outils (Pelle/Hache/Pioche/Houe)
- Ajout d'objets de combats (Épée, Armure)
- Effets de potions appliqués lorsqu'un set d'armure est entièrement porté
- Tissage (Application d'un effet de potion lors de l'utilisation d'une gemme)

## Stack technique

- Langage de progammation : Java 21
- API de modification : Fabric (Quilt prévu)
- Gestion de dépendance : Gradle

## Code source/Téléchargement

Le code est disponible sur [GitHub](https://github.com/xotakfr/simple-gems-fabricated)

Le mod est téléchargable sur [Modrinth (préférence)](https://modrinth.com/mod/simple-gems-fabricated) ou sur [CurseForge](https://www.curseforge.com/minecraft/mc-mods/simple-gems-fabricated)