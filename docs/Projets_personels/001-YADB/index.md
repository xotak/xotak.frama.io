---
tags:
   - Node.JS
   - JavaScript
   - Application
---

# YADB

YADB est une application intégrée à Discord qui permet de simplifier l'administration d'un serveur Discord, jouer de la musique en salon vocal, gagner des niveaux, obtenir vos statistiques sur des jeux Hoyoverse (intégration Enka.Network) et plus !

## Fonctionnalités

- Gestion des sanctions utilisateurs (Kick/Ban/Timeout)
- Système de gain de niveau (Expérience & Niveaux, classement des utilisateurs)
- Musique (Lecture/Pause/Suivant/Précédent/Suggestion de musique)
- Intégration Enka.Network (Statistiques Genshin Impact/Homkai: Star Rail) (en cours)
- Récupération d'images en ligne (Reddit/APIs)
- Sharding (Utilisation de plusieurs processus pour servir plus d'utilisateurs)

## Stack technique

- Langage de programmation : JavaScript
- Runtime : Node.JS (Non testé avec bun et deno)
- Intégration Discord : discord.js@14
- Serveur musique : [NodeLink](https://github.com/PerformanC/NodeLink)
- Intégration serveur musique : [moonlink.js@v4](https://github.com/Ecliptia/moonlink.js)
- Intégration Enka.Network: [enkanetwork.js](https://github.com/Jelosus2/enkanetwork.js)
- Base de données : `sqlite3`
- Connection à la base de données : Sequelize
- Logging : `pino` et `pino-pretty`

## Source

Disponible sur [Framagit](https:/framagit.org/xotak/yadb) sous license CECiLL-2.1

