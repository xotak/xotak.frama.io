---
tags:
    - Autres
---

# Contributions diverses

## Traductions

### Grasscutter

J'ai participé à l'implémentation de la traduction francaise d'un logiciel appelé Grasscutter, qui est un serveur local pour ["Un certain jeu d'animé"](https://fr.wikipedia.org/wiki/Genshin_Impact). Les clés de textes sont stockés dans un fichier JSON ,et j'ai également traduit le fichier lisez-moi en francais. J'ai par la même occasion développé ma connaissance de l'outil git, pour la collaboration sur des projets avec des dizaines de personnes actives. Je compte également contribuer au développement du projet, dès que j'aurais acquis assez de connaissances en Java

### Lucky Patcher

[Lucky Patcher](https://fr.wikipedia.org/wiki/Lucky_Patcher) est une application de manipulation de fichiers apk, proposant de diverses fontionnalité comme l'émulation des achats in app, ainsi que le blocage de pub ou la suppression de vérification de licences. Le projet de traduction est réalisé sur transifex, ce qui est bien plus pratique que Grasscutter

## Autres

### Beta test - Spiritfarer

J'ai participé au deux phases de beta fermées d'un jeu appelé Spiritfarer, développé par Thunderlotus, un studio Québécois. J'ai su produire un retour d'expérience sur le jeu facilement