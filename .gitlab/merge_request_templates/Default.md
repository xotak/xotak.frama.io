## Description

Veuillez lire atentivement la [note de contribution](https://framagit.org/loulou310/loulou310.frama.io/-/blob/main/CONTRIBUTING.md) avant de d'ouvrir une PR.

## Problèmes corrigés avec cette PR

<!-- Mentionnez une issue ici si elle est en rapport avec cette PR -->

## Type de changement

<!--- Mettez un `x` dans la case qui correspond a votre changement. -->

- [ ] Contenu
- [ ] Backend

## Checklist:

- [ ] La PR est unique et il n'y en a pas d'autre PR qui est ouverte avec les même changements.
- [ ] J'ai lu la [note de contribution](https://framagit.org/loulou310/equipe-info/-/blob/main/CONTRIBUTING.md).
- [ ] Je suis responsable de tout les problèmes de copytight avec le contenu que j'ajoute si cela arrive dans le futur.